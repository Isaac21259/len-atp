{-# LANGUAGE RankNTypes #-}
module Main where
import Control.Lens hiding (children)
import Control.Applicative
import Control.Lens.Extras (is)
import Data.Maybe
import Text.Read

data Expr = And Expr Expr
          | Or Expr Expr
          | Implies Expr Expr
          | Not Expr
          | Tt
          | Ff
          | Var Int
          deriving (Eq, Show, Read)

eval :: Expr -> Maybe Bool
eval (And a b) = do
  a' <- eval a
  b' <- eval b
  return $ a' && b'
eval (Or a b) = do
  a' <- eval a
  b' <- eval b
  return $ a' || b'
eval (Implies a b) = do
  a' <- eval a
  b' <- eval b
  return $ not a' || b'
eval (Not a) = do
  a' <- eval a
  return $ not a'
eval Tt = return True
eval Ff = return False
eval (Var _) = Nothing

_Var :: Prism' Expr Int
_Var = prism Var $
      \tm -> case tm of
               Var v -> Right v
               _ -> Left tm

_Expr :: Prism' String Expr
_Expr = prism' show readMaybe

children :: Traversal' Expr Expr
children = traversal go
  where go focus (And a b) = liftA2 And (focus a) (focus b)
        go focus (Or a b) = liftA2 Or (focus a) (focus b)
        go focus (Implies a b) = liftA2 Implies (focus a) (focus b)
        go focus (Not a) = Not <$> focus a
        go focus Tt = pure Tt
        go focus Ff = pure Ff
        go focus (Var i) = pure (Var i)

descendants :: Traversal' Expr Expr
descendants = traversal go
  where go focus Tt = focus Tt
        go focus Ff = focus Ff
        go focus (Var i) = focus (Var i)
        go focus tm = (children . descendants) focus tm

varLessChildren :: Traversal' Expr Expr
varLessChildren = traversal go
  where go focus tm = (children . filtered (hasn't vars)) focus tm

varLessBranches :: Traversal' Expr Expr
varLessBranches = deepOf children varLessChildren

vars :: Traversal' Expr Expr
vars = descendants . filtered (is _Var)

var :: Int -> Traversal' Expr Expr
var i = descendants . filtered (\tm -> preview _Var tm == Just i)

subs :: Expr -> [Expr]
subs e = case firstOf vars e of
            Just (Var i) -> do
              v <- [Tt, Ff]
              let r = over (var i) (const v) e
              subs r
            _ -> return e

valid :: Expr -> Bool
valid e = and $ catMaybes $ eval <$> subs e

main :: IO ()
main = do
  putStr "Please enter a boolean formula: "
  l <- getLine
  putStr "Basic Simplification: "
  putStrLn $ over (_Expr . varLessBranches) (\tm -> if valid tm then Tt else Ff) l
  putStr "Tautology: "
  putStrLn $ over _Expr (\tm -> if valid tm then Tt else Ff) l
